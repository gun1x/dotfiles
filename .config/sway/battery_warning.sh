#!/bin/bash

while true; do
  battery_percentage="$((`cat /sys/class/power_supply/BAT0/energy_now` * 100 / `cat /sys/class/power_supply/BAT0/energy_full_design`))"

  if (( $battery_percentage < 20 && $battery_percentage > 8 )); then
    swaynag --font "Ubuntu 30" -t warning -m "Dude my batter is at ${battery_percentage}%. You ready to charge me?"
  fi
  if (( $battery_percentage < 9 )); then
    swaynag --font "Ubuntu 30" -t warning -m "My battery is at ${battery_percentage}%. I will fuck up your work."
  fi

  sleep 300
done
