#!/bin/bash
while true; do
  swaymsg output "*" background $(find ~/docs/wallpapers/ -type f | sort -R | head -1 ) fill
  sleep 1000
done
