#!/bin/bash
echo -e "\e[1m\e[34m$(date)"
echo -e "\e[0m\e[36m
Using workspace $(i3-msg -t get_workspaces \
  | jq '.[] | select(.focused==true).name' \
  | cut -d"\"" -f2)"

echo -e "\e[30m\e[1m"

acpi

echo -e "\e[0m"

getContainerForStatus() {
  result="$(docker ps "--filter=status=$1" --format '{{.Names}}')"
  count="$(printf "%s" "$result" | wc -w)"
  if [ "$count" != "0" ] ; then
    echo -e "\e[1m\e[33m$count \e[0m\e[31mcontainers \e[1m\e[34m${1}\e[0m: \e[32m$(printf "%s" "$result" | cut -d _ -f1 | sort | uniq -c | xargs)"
  fi
}

displayDockerInfo() {
  getContainerForStatus created
  getContainerForStatus dead
  getContainerForStatus exited
  getContainerForStatus paused
  getContainerForStatus removing
  getContainerForStatus restarting
  getContainerForStatus running
}

docker ps > /dev/null 2>&1 && displayDockerInfo || echo "Docker is stopped."

echo

getIPs () {
  connName="$(echo "$@" | cut -d":" -f1 )"
  connType="$(echo "$@" | cut -d":" -f3 )"
  deviceName="$(echo "$@" | cut -d":" -f4 )"
  ips=$(ip -4 -br a s "$deviceName" | sed -re 's,\s+, ,g' | cut -d ' ' -f 3- )
  echo -e "\e[0m\e[31m$connName:\e[1m\e[32m$connType:\e[0m\e[33m$deviceName:\e[1m\e[34m$ips"
}


# shellcheck disable=SC2086
{
  echo -e "\e[0m\e[36mCONN:\e[1m\e[35mTYPE:\e[0m\e[36mDEVICE:\e[1m\e[36mIPs"
  nmcli -t connection show --active | while read -r line ; do getIPs $line ; done
} | column -s':' -t


echo

# shellcheck disable=SC2086
{
memoryOutput="$(free -m | grep Mem)"
allMemory="$( echo $memoryOutput | cut -d ' ' -f 2 )"
freeMemory="$( echo $memoryOutput | cut -d ' ' -f 4 )"
echo -e "\e[0m\e[1mMemory"\
  "\e[0m\e[31mused\e[1m" "$( echo $memoryOutput | cut -d ' ' -f 3 )" \
  "\e[0m\e[34m+buff\e[1m" "$(( allMemory - freeMemory ))" \
  "\e[0m\e[32mfree\e[1m" "$freeMemory"
}

echo -e "\e[36m"

sudo ps -Ao rss,comm |awk '{print $1 " " $2 }'| awk '{tot[$2]+=$1;count[$2]++} END {for (i in tot) {print tot[i],i,count[i]}}' | sort -n | tail -5 | numfmt --field=1 --to-unit=K > /tmp/myStatusReport1

sudo ps -Ao pcpu,comm | awk '{tot[$2]+=$1;count[$2]++} END {for (i in tot) {print tot[i],i,count[i]}}'  | sort -h | tail -5 > /tmp/myStatusReport2

echo "MEM App Nr | CPU App Nr
$(sdiff /tmp/myStatusReport1 /tmp/myStatusReport2)" | column -t -R 1,2,3,4,5,6
rm /tmp/myStatusReport1 /tmp/myStatusReport2

echo

echo -e "\e[32mTemperature: \e[1m\e[32m$(sensors | awk '/Core/ { print $3 }' | cut -d '.' -f1 | cut -d '+' -f2 | xargs)"

echo -e "\e[36m"

spotifyStatus="$(dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:org.mpris.MediaPlayer2.Player string:PlaybackStatus 2>&1)"
#shellcheck disable=SC2181
if [ "$?" != "0" ] > /dev/null; then
  echo "spotify not running"
else
  songInfo="$(dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:org.mpris.MediaPlayer2.Player string:Metadata | grep -i -e albumArtist -e album -e title -A 3 | grep variant -A 1 | grep string | cut -d '"' -f2)"
  echo -e "$( printf "%s" "$spotifyStatus"  | tail -1 | cut -d'"' -f2)" \
  "\e[0m\e[32m$(echo "$songInfo" | tail -2 | head -1): \e[34m$(echo "$songInfo" | head -1)"
  echo -e "        \e[1m\e[35m$(echo "$songInfo" | tail -1)"
fi
