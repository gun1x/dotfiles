userName=$(whoami)
grep "Arch Linux" /etc/issue > /dev/null
if [[ $? == 0 ]]; then
	sudo pacman -S powerline zsh
	curl -o "/home/$userName/.zshrc" https://gitlab.com/gun1x/scripts/raw/master/.zshrc
else
	sudo apt install zsh powerline fonts-powerline -y
	curl -o "/home/$userName/.zshrc" https://gitlab.com/gun1x/scripts/raw/master/.zshrc-deb
fi
sudo cp "/home/$userName/.zshrc" /root/
sudo usermod -s $(which zsh) $userName
sudo usermod -s $(which zsh) root
